def get_scores_for_label(y_true, y_pred, label):
    tp = 0
    num_hypo = 0
    num_ref = 0
    for hypo, ref in zip(y_pred, y_true):
      if hypo == label:
        num_hypo += 1
        if hypo == ref:
          tp += 1
      if ref == label:
        num_ref += 1
    precision = tp / num_hypo
    recall = tp / num_ref
    f1 = 2 * precision * recall / (precision + recall)
    return precision, recall, f1

def evaluate(y_true, y_pred):
    labels = ["ABUSE", "INSULT", "PROFANITY", "OTHER"]
    sum_p = 0
    sum_r = 0
    for label in labels:
      p, r, f1 = get_scores_for_label(y_true, y_pred, label)
      print(label + ": " 
            + "P = " + str(format(p, '.2f')) 
            + ", R = " + str(format(r, '.2f')) 
            + ", F1 = " + str(format(f1, '.2f')))
      sum_p += p
      sum_r += r
    avg_p = sum_p / len(labels)
    avg_r = sum_r / len(labels)
    avg_f1 = 2 * avg_p * avg_r / (avg_p + avg_r)
    print("average: "
          + "P = " + str(format(avg_p, '.2f')) 
          + ", R = " + str(format(avg_r, '.2f')) 
          + ", F1 = " + str(format(avg_f1, '.2f')))
    return avg_f1