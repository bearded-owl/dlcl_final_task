# -*- coding: utf-8 -*-

# Deep Learning in der Computerlinguistik
# Autor: Fabian Weiersmüller, s11717071, 2019

import helper_functions as hf
import numpy
from keras.preprocessing.sequence import pad_sequences

import keras.utils as kutils
from keras import regularizers
from keras.models import Sequential
from keras.layers import Dense, Embedding, Conv1D, GlobalMaxPooling1D
import pickle

train_data_path = 'hate_speech/hate_speech.train'
test_data_path = 'hate_speech/hate_speech.test'
model_path = 'hate_speech/model_twitter_classification'
prediction_path = 'hate_speech/model_twitter_predictions.txt'

# embedding of german words from: https://deepset.ai/german-word-embeddings
# 300 dimensions
embedding_path = 'hate_speech/glove_word_embeddings.txt'


USE_EXTERNAL_EMBEDDING = True
PREDICT_STUFF = True

print('Starting twitter classification.')
(id_train, x_train, y_train) = hf.load_train_data(train_data_path)
(id_test, x_test) = hf.load_test_data(test_data_path)
print('Data loaded.')

print('Preprocess sentences.')
x_train = hf.preprocess_data(x_train)
x_test = hf.preprocess_data(x_test)
print('Preprocessing done.')

embeddings = {}
MAX_WORDS = 16000  # our data set only contains 16k words

bag_of_words = {}
for line in x_train:
        for token in line:
            if token in bag_of_words:
                bag_of_words[token] += 1
            else:
                bag_of_words[token] = 1

# -1 because we want indices from 2 to 16000
most_common_words = sorted(bag_of_words.items(), key=lambda tup: tup[1], reverse=True)[0:MAX_WORDS-1]
bag_of_words = {tup[0]: i+2 for i, tup in enumerate(most_common_words)}
# now we have the MAX_WORDS - most common words in our training data
# mapping word: index, index being if its the most common word (=2) or the 275th most common word (=276) etc
# 0 is used for padding, 1 is used for unknown words

print('About to transforme sentences from string to int ... ')
x_train = hf.transform_words_to_int(x_train, bag_of_words)
x_test = hf.transform_words_to_int(x_test, bag_of_words)
print('Transforming done')

# no twitter message seems to have more than ~80 tokens, so wo take 85
# 0-value is padding
x_train = pad_sequences(x_train, maxlen=85, dtype='int32', padding='post', truncating='post', value=0)
x_test = pad_sequences(x_test, maxlen=85, dtype='int32', padding='post', truncating='post', value=0)
print('Padding done.')


if PREDICT_STUFF:
    print('Attempt to unpickle model-file.')
    with open(model_path, 'rb') as model_file:
        model = pickle.load(model_file)
    print('Unpickling successful.')

    classes = model.predict(x_test, batch_size=100)
    with open(prediction_path, 'wt') as results_file:
        for i, elem in enumerate(classes):
            predicted_label = hf.give_predict_label(elem)
            results_file.write(id_test[i] + '\t' + predicted_label + '\n')
    print('Wrote results to file.')
    print('DONE')

    for elem in classes:
        print(elem)

else:
    # Since we want to make multi-class classification, we need a softmax in the last layer,
    y_train = hf.labels_to_int(y_train)
    y_train_onehot = kutils.to_categorical(y_train, num_classes=4, dtype='int32')
    print('One-hot vectors created.')

    # Alright, let's build a model
    model = Sequential()
    print('model - object initialized.')

    # Embedding-matrix contains the vectors of those words that were in our data set and also precomputed
    # +1 for the unknown words
    embedding_matrix = numpy.zeros((MAX_WORDS + 1, 300))
    # unknown words, we set vector to random
    embedding_matrix[1] = numpy.random.random_sample((1, 300))

    if USE_EXTERNAL_EMBEDDING:

        # we take 100k words from the embedding, for memory reasons split in 2 parts

        with open(embedding_path, mode='rt', encoding='utf8') as embedding_file:
            for i, line in enumerate(embedding_file):
                if i > 50000:
                    break
                if i % 1000 == 0:
                    print('.', end='')
                vals = line.split(' ')
                embeddings[vals[0]] = vals[1:]
            print()

            for word, ind in bag_of_words.items():
                embedding_vector = embeddings.get(word)
                if embedding_vector is not None:
                    # words in our training-data not found in the pre-computed embeddings will be all-zeros.
                    embedding_matrix[ind] = embedding_vector

            del embeddings
            embeddings = {}

            for i, line in enumerate(embedding_file):
                if i > 50000:
                    break
                if i % 1000 == 0:
                    print('.', end='')
                vals = line.split(' ')
                embeddings[vals[0]] = vals[1:]
            print()

            for word, ind in bag_of_words.items():
                embedding_vector = embeddings.get(word)
                if embedding_vector is not None:
                    # words in our training-data not found in the pre-computed embeddings will be all-zeros.
                    embedding_matrix[ind] = embedding_vector

            del embeddings

        model.add(Embedding(input_dim=MAX_WORDS + 1, output_dim=300, input_length=85,
                            weights=[embedding_matrix], trainable=False))
        print('External embeddings done.')
    else:
        model.add(Embedding(input_dim=MAX_WORDS+1, output_dim=300, input_length=85,
                            embeddings_initializer='random_uniform'))
        print('Own embeddings done.')

    model.add(Conv1D(filters=100, kernel_size=5, activation='tanh', input_shape=(85, 300), use_bias=True))
    print('Added convolution layer.')
    # Max-Pooling after the convolution
    model.add(GlobalMaxPooling1D())
    print('Added max-pooling.')

    # Next, we put a fully-connected hidden layer with 100 hidden units and tanh activation.
    model.add(Dense(units=100, activation='tanh', input_dim=100))
    print('Added hidden layer.')

    # softmax output layer.
    model.add(Dense(units=4, activation='softmax'))
    print('Added softmax.')

    # - - - - - - - - - - - - - - - - - - - - - - - - - -

    print('About to compile the model ...')
    model.compile(loss='categorical_crossentropy', optimizer='sgd', metrics=['accuracy'])
    print('Model compiled!')

    print('About to start fitting ...')
    model.fit(x_train, y_train_onehot, epochs=15, batch_size=30, validation_split=0.1)
    print('')
    print('Fitting done!')
    print('About to pickle model-file...')
    with open(model_path, 'wb') as pickleFile:
        pickle.dump(model, pickleFile)
    print('pickling done.')
    print('DONE')
