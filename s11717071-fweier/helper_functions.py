# -*- coding: utf-8 -*-

# Deep Learning in der Computerlinguistik
# Autor: Fabian Weiersmüller, s11717071, 2019

import nltk

# found at https://pypi.org/project/tweet-preprocessor/
import preprocessor

# found at https://pypi.org/project/emoji/
import emoji
EMOJIS = emoji.EMOJI_UNICODE.values()


def load_train_data(path):
    ids = []
    messages = []
    tags = []
    with open(path, mode='rt', encoding='utf-8') as infile:
        for line in infile:
            (i, m, t) = line.split('\t')
            ids.append(i)
            messages.append(m)
            tags.append(t)
    return ids, messages, tags


def load_test_data(path):
    ids = []
    messages = []
    with open(path, mode='rt', encoding='utf-8') as infile:
        for line in infile:
            (i, m) = line.split('\t')
            ids.append(i)
            messages.append(m)
    return ids, messages


def remove_duplicates_and_regularize(line: str) -> str:
    """
    Method removes duplicate emojis and all letters after the second
    looooooool --> lool ; :D:D:D:D --> :D ; blabla.......bla --> blabla... bla
    :param line: twitter-line
    :return: string with all duplicate (and above) emojis and all tripple-letters (and above) removed
    """
    clean_line = ''
    prev_char = ''
    prevprev_char = ''
    emois = emoji.EMOJI_UNICODE.values()
    for i, char in enumerate(line):
        if i > 1:
            if char in EMOJIS:
                if char == prev_char:
                    # skip double-emojis
                    pass
                else:
                    # normal char or two different emojis
                    if prev_char != ' ':
                        # separate emojis from text / each other
                        clean_line += ' '

                    clean_line += char
            else:
                if char in ['.', ':', ',', '!', '?', ';', '\"', '\'']:
                    pass
                elif char == prev_char and char == prevprev_char:
                    pass
                else:
                    clean_line += char
        else:
            # We're not changing the first 2 chars
            clean_line += char

        prevprev_char = prev_char
        prev_char = char

    return clean_line


def remove_emojis(line):
    """
    Method to remove all emojis from text
    :param line:
    :return:
    """
    clean_line = ''
    for char in line:
        if char in EMOJIS:
            pass
        else:
            clean_line += char
    return clean_line


def preprocess_data(sentences):
    """
    our messages are VERY noisy. We need to pre-process them.
    :param sentences: unprocessed sentences
    :return: sentences, where:
                                - sentence is lowercase
                                - twitter-tags are removed
                                - emojis are normalized
                                - duplicates are removed
                                - linebreaks are removed
    """
    for i, line in enumerate(sentences):
        line = line.replace('|LBR|', '')
        line = remove_duplicates_and_regularize(line)
        # line = remove_emojis(line)
        line = emoji.demojize(line)
        line = preprocessor.clean(line)
        line = line.lower()
        line = line.replace(':', '_')
        # tokenize
        token_maker = nltk.TweetTokenizer()
        line = token_maker.tokenize(line)
        sentences[i] = line
        if i % 200 == 0:
            print('.', end='')
    print('')
    return sentences


def transform_words_to_int(sentences, word_dict):
    """
    Method transforms a list of string-tokens into a list of integers
    :param sentences: list of sentences
    :param word_dict: dictionary, containing {word: int} mapping
    :return: sentenses, with each token replaced by the corresponding int from the dict
    """
    for i, sentence in enumerate(sentences):
        new_sentence = []
        for token in sentence:
            try:
                new_sentence.append(word_dict[token])
            except KeyError:
                # unknown token, we set the id to 1. Padding is 0 and most common word is 2.
                new_sentence.append(1)
        sentences[i] = new_sentence
    return sentences


def labels_to_int(labels):
    """
    Method turns list of labels into list of integers, representing those labels
    :param labels: list of strings
    :return: list of integers indicating label
    """
    values = ['INSULT', 'ABUSE', 'PROFANITY', 'OTHER']
    for i, label in enumerate(labels):
        labels[i] = values.index(label.strip())
    return labels


def give_predict_label(probabilities):
    """
    Method returns most likely label, given the probabilities
    :param probabilities: tuple containing probabilities for the 4 labels
    :return: string, the predicted label
    """
    values = ['INSULT', 'ABUSE', 'PROFANITY', 'OTHER']
    probabilities = list(probabilities)
    max_value = max(probabilities)
    max_index = probabilities.index(max_value)
    return values[max_index]
