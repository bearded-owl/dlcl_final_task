# -*- coding: utf-8 -*-

# Deep Learning in der Computerlinguistik
# Autor: Fabian Weiersmüller, s11717071, 2019

from keras.datasets import imdb
import keras.preprocessing as kprepro
import keras.utils as kutils
from keras.models import Sequential
from keras.layers import Dense, Embedding, SimpleRNN, Flatten, GRU, LSTM, Bidirectional
import pickle

MODEL_TO_TRY = 2  # possible values: 0 - 4

print('start keras demo file ! :D')
# x_train, x_test: list of sequences, which are lists of indexes (integers).
# (most common word ---> index 1, etc. unknown word uses index 0)
# y_train, y_test: list of integer labels (1 or 0)

# num_words: only consider the top x words, all other words become the oov_char
# skip_top: ignore the top x most frequent words. They become the oov_char
# start_char: Mark the start of a sequence with this char. Take 1, since 0 is by convention the padding character
(x_train, y_train), (x_test, y_test) = imdb.load_data(path="imdb.npz", num_words=30000, skip_top=0,
                                                      maxlen=100, seed=113, start_char=1, oov_char=0)
# pad all sequences to max_len, value is the value used for padding, by convention it is the number 0.
x_train_padded = kprepro.sequence.pad_sequences(x_train, maxlen=100, dtype='int32',
                                                padding='post', truncating='post', value=0)
x_test_padded = kprepro.sequence.pad_sequences(x_test, maxlen=100, dtype='int32',
                                               padding='post', truncating='post', value=0)
print('Data padding done.')

"""
Changes values in, e.g. x_train from:
array([list([1, 14, 22, ..., 16, 5345, 19, 178, 32]),
       list([1, 194, 1153, ..., 16, 145, 95]),
       list([1, 14, 47, ..., 7, 129, 113]),
       ...,
       list([1, 518, 21, ..., 28, 1816, 98]),
       list([1, 14, 22, ..., 158, 10, 10]),
       list([1, 13, 219, ..., 220, 484, 867])],
      dtype=object)

(all different length !!)      
to:
array([[   1,   14,   22, ...,    0,    0,    0],
       [   1,  194, 1153, ...,    0,    0,    0],
       [   1,   14,   47, ...,    0,    0,    0],
       ...,
       [   1,  518,   21, ...,    0,    0,    0],
       [   1,   14,   22, ...,    0,    0,    0],
       [   1,   13,  219, ...,    0,    0,    0]], dtype=int32)
"""

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# Since we want to make multi-class classification, we need a softmax in the last layer,
# we therefore transform the y_train and y_test labels to one-hot vectors


# y: class vector to be converted into a matrix (integers from 0 to num_classes).
# num_classes: total number of classes.
# dtype: The data type expected by the input, as a string (float32, float64, int32...)
y_train_onehot = kutils.to_categorical(y_train, num_classes=2, dtype='int32')
y_test_onehot = kutils.to_categorical(y_test, num_classes=2, dtype='int32')
print('One-hot vectors created.')

"""
Example:

An array of 5 labels out of a set of 3 classes {0, 1, 2}:

array([0, 2, 1, 2, 0])

'to_categorical' converts this into a matrix with as many columns as there are classes.
The number of rows stays the same.
> to_categorical(labels)
array([[ 1.,  0.,  0.],
       [ 0.,  0.,  1.],
       [ 0.,  1.,  0.],
       [ 0.,  0.,  1.],
       [ 1.,  0.,  0.]], dtype=float32)
"""

# Alright, build rnn - models (see which one is better)
model_1 = Sequential()
model_2 = Sequential()
model_3 = Sequential()
model_4 = Sequential()
model_5 = Sequential()
model_list = [model_1, model_2, model_3, model_4, model_5]
print('model - objects initialized.')

# Step "0": layer for learning 100 - dimensional embeddings
# Benefit: In our input data, we only need to give the index of each word in our vocabulary,
# which is very efficient and flexible

# input_dim: int > 0. Size of the vocabulary, i.e. maximum integer index + 1. Set by us to be 30000 further above
# output_dim: We want our embedding-vector to have 100 dimensions
# input_length: length of sentence (input sequence), we set this to 100 further above
# embedded = Embedding(vocab_size, embedding_dim, input_length=seq_length)
model_list[MODEL_TO_TRY].add(
    Embedding(input_dim=30000, output_dim=100, input_length=100, embeddings_initializer='random_uniform'))
print('Embeddings done.')


# actual models start here ...
# Five different RNNs, for all: 'input_shape=(500, 100)' is needed, because we later connect a dense layer
# to our network. We need the info to calculate its size.

if MODEL_TO_TRY == 0:
    # Standard recurrent layer with 100 hidden units and tanh activation
    model_1.add(SimpleRNN(units=100, return_sequences=True, activation='tanh',
                          input_shape=(500, 100), use_bias=True, batch_size=50))
elif MODEL_TO_TRY == 1:
    # Recurrent layer with gated recurrent units ('GRU') with 100 hidden units and tanh activation
    model_2.add(GRU(units=100, return_sequences=True, activation='tanh',
                    input_shape=(500, 100), use_bias=True, batch_size=50))
elif MODEL_TO_TRY == 2:
    # Recurrent layer with long-short term memory units ('LSTM') with 100 hidden units and tanh activation
    model_3.add(LSTM(units=100, return_sequences=True, activation='tanh',
                     input_shape=(500, 100), use_bias=True, batch_size=50))
elif MODEL_TO_TRY == 3:
    # About bidirectional wrapper in keras:
    # This will create two copies of the hidden layer, one fit in the input sequences as-is
    # and one on a reversed copy of the input sequence.
    # By default, the output values from these LSTMs will be concatenated.

    # Bi-directional recurrent layer with long-short term memory units ('bi-LSTM') with 50 hidden units for
    # each direction and tanh activation. Use the default variant concatenation for merging the two directions.
    model_4.add(Bidirectional(LSTM(units=50, activation='tanh',input_shape=(500, 100)),
                              merge_mode='concat', batch_size=50))
elif MODEL_TO_TRY == 4:
    # Bi-directional recurrent layer with long-short term memory units ('bi-LSTM') with 100 hidden units for
    # each direction and tanh activation. Use the default variant concatenation for merging the two directions.
    model_5.add(Bidirectional(LSTM(units=100, activation='tanh', input_shape=(500, 100)),
                              merge_mode='concat', batch_size=50))
else:
    print('Model number does not exist. Try 0 to 4.')
    exit()


# Next, we put a fully-connected hidden layer with 100 hidden units and tanh activation.
# Do I need input-dim ? Let's ust try to let it away ... Ok, does not work, I need to flat the input

model_list[MODEL_TO_TRY].add(Flatten())
model_list[MODEL_TO_TRY].add(Dense(units=100, activation='tanh'))
print('Added hidden layer.')

# softmax output layer. Our movie-reviews are labled "positive", represented as 1, or "negative", represented as '0'
# meaning 2 classes to differentiate
model_list[MODEL_TO_TRY].add(Dense(units=2, activation='softmax'))
print('Added softmax.')

# - - - - - - - - - - - - - - - - - - - - - - - - - -

# Alright, now we have to "compile" our model
# optimizer is "stochastic gradient descent"
# loss-function is "categorical crossentropy"
# we evaluate the accuracy on-the-fly during training
print('About to compile the model ...')
model_list[MODEL_TO_TRY].compile(loss='categorical_crossentropy', optimizer='sgd', metrics=['accuracy'])
print('Model compiled!')


# output_shapes = [layer.output_shape for layer in model_list[MODEL_TO_TRY].layers]
# print(repr(output_shapes))


# Now, for the first time, we acutally use our data. We train for 5 epochs, batch-size 100
# validation_split: The fit function allows to automatically monitor the model on a development set
# (using the metrices defined when compiling the model)
# We use a split of 20%
print('About to start fitting ...')
model_list[MODEL_TO_TRY].fit(x_train_padded, y_train_onehot, epochs=5, batch_size=100, validation_split=0.2)
print('')
print('Fitting done!')
print('About to pickle model-file...')
with open(
        '/Users/fabian_weiersmueller/Desktop/UNI/Informatik/Deep Learning in CL/Übung 8 - Coding-Task/model_rnn_' +
        str(MODEL_TO_TRY), 'wb') as pickleFile:
    pickle.dump(model_list[MODEL_TO_TRY], pickleFile)
print('pickling done.')

print('About to evaluate model ...')
# Finally we evaluate our model:
loss_and_metric = model_list[MODEL_TO_TRY].evaluate(x_test_padded, y_test_onehot, batch_size=128)
print('Evaluation done. About to pickle evaluation results ...')

with open('/Users/fabian_weiersmueller/Desktop/UNI/Informatik/Deep Learning in CL/Übung 8 - Coding-Task/' +
          'loss_and_metric_rnn_' + str(MODEL_TO_TRY), 'wb') as pickleFile:
    pickle.dump(loss_and_metric, pickleFile)

print('Done')
print('- - - - - - - - - - - - - - - - -')
print(repr(loss_and_metric))
